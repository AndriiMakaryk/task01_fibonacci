package task01_Fibonacci;

import java.util.Scanner;

public class FibonacciTester {

	public static void main(String[] args) {
		String enteredValue;
		int chose = 0;
		int startInterval = 0;
		int endInterval = 0;
		int sizeOfSet = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println();
		System.out.println("================Welcome to First task==================");
		System.out.println("Please enter the first value from starting interval and press Enter: ");
		enteredValue = scanner.nextLine();
		try {
			startInterval = Integer.parseInt(enteredValue);
		} catch (NumberFormatException exception) {
			exception.printStackTrace();
		}
		System.out.println("Please enter the second value from end of interval and press Enter: ");
		enteredValue = scanner.nextLine();
		try {
			endInterval = Integer.parseInt(enteredValue);
		} catch (NumberFormatException exception) {
			System.out.println("Entered value uncorrect.");
		}
		do {
			System.out.println();
			System.out.println("========================Menu============================");
			System.out.println("Print all odd numbers of this interval (Press 1)");
			System.out.println("Print the sum odd and even numbers of this interval (Press 2)");
			System.out.println("Print the biggest odd and biggest even numbers of this Fibonacci numbers(Press 3)");
			System.out.println("Print percentage of odd and even Fibonacci numbers(Press 4)");
			System.out.println("Exit(Press 5)");
			enteredValue = scanner.nextLine();
			FibonacciMethods tester = new FibonacciImplementation();
			try {
				chose = Integer.parseInt(enteredValue);
			} catch (Exception ex1) {
				System.out.println("Wrong number format.");
			}
			if (chose == 1) {
				tester.printSequance(startInterval, endInterval);
			} else if (chose == 2) {
				tester.calculateSum(startInterval, endInterval);
			} else if (chose == 3) {
				System.out.println("Please enter the size of set and press Enter: ");
				enteredValue = scanner.nextLine();
				try {
					sizeOfSet = Integer.parseInt(enteredValue);
				} catch (Exception ex2) {
					System.out.println("Wrong number format.");
				}
				tester.printBiggestNumbers(sizeOfSet);

			} else if (chose == 4) {
				System.out.println("Please enter the size of set and press Enter: ");
				enteredValue = scanner.nextLine();
				try {
					sizeOfSet = Integer.parseInt(enteredValue);
				} catch (Exception ex) {
					System.out.println("Wrong number format.");
				}
				tester.percentageOfOddAndEven(sizeOfSet);
			}

		} while (!enteredValue.equals("5"));
		scanner.close();

	}

}
